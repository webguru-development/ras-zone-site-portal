$(document).ready(function(){
  var gal = $('#QuestionCarousel');
  gal.owlCarousel({
    items:2,
    autoplay: false,
    loop:false,
    margin:0,
    rewind: true,
    lazyLoad:true,
    dots: false,
    nav: true,
    navText : ['<i class="icon icon-arrow-thin-left"></i>','<i class="icon icon-arrow-thin-right"></i>'],
    responsive:{
      0: {
        items: 1,
      },
      440: {
        items: 1,
      },
      500: {
        items: 1,
      },
      600: {
        items: 1,
      },
      767: {
        items: 1,
      },
      999: {
        items: 2,
      },
      1100: {
        items: 2,
      },
      1200: {
        items: 2
      }
    },
  });
  $('body').prepend( $( '<div class="layoutovarlay"></div>' ) );

  // Filter Toggle
  $('body').prepend( $( '<div class="layoutovarlay3"></div>' ) );
  $(".ProfileBtn").click(function () {
    $("body").toggleClass('layout-expanded3');
    });
    $('.layoutovarlay3').on('click', function(e){
      e.preventDefault();
      if($("body").hasClass('layout-expanded3')){
        $("body").removeClass('layout-expanded3');
      }
    });
  // 08062020
  $("#UserNav").click(function(){ 
    $("#UserNavBody").slideToggle("fast");
  });
  // Sticky Navigation
  let lastScrollTop = 0;
  document.addEventListener("scroll", function(e){ 
    let header = document.querySelector('.headerinner')
    let st = window.pageYOffset || document.documentElement.scrollTop; 
    if (st > lastScrollTop){
      if(window.pageYOffset > 0){
        header.classList.add('sticky'); 
     }
     if(window.pageYOffset > 500){
         header.classList.add('sticky-hide');
     }
      //console.log('down');
     } else {
       if(window.pageYOffset == 0){
          header.classList.remove('sticky'); 
       }else {
          header.classList.remove('sticky-hide');
       }
        // console.log('up')
     }
     lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
  }, false);

  $(document).ready(function(){
     var height = $('.headerinner').outerHeight();
      $('.headercontainer').css('min-height',height);
    $(window).resize(function(){
      var height = $('.headerinner').outerHeight();
      $('.headercontainer').css('min-height',height);
    });
  });
});