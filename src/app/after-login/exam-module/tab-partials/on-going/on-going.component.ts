import { Component, OnInit } from '@angular/core';
import {OnGoingService} from './on-going.service'
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, ParamMap  } from '@angular/router';

@Component({
  selector: 'app-on-going',
  templateUrl: './on-going.component.html',
  styleUrls: ['./on-going.component.scss']
})
export class OnGoingComponent implements OnInit {

  public user_data:any;
  public pageData: any = [];
  public sortBy: number = 5;
  public searchText: string = '';
  public blank: any = ' ';
  public _userId:any='';
  public _accessToken:any='';
  public currentPageNumber: number = 1;
  public dataPerPage: number = 0;
  public totalData: number = 0;

  
  private _activatedRouteQueryParamsSubscribe: any;
  constructor(private _ongoingService: OnGoingService,private toastr: ToastrService,private _activatedRoute: ActivatedRoute,private _router: Router) {
    this._activatedRouteQueryParamsSubscribe = this._activatedRoute.queryParams.subscribe(queries => {
      if(typeof queries.search_text !== 'undefined' && queries.search_text != null) {
        this.searchText = queries.search_text;
      }
    })

  }

  ngOnInit(): void {
    let userDetails = JSON.parse(localStorage.getItem("user_data"));
    this._userId = userDetails.user_id;
    this._accessToken = userDetails.access_token;
    this.ongoingexampagedata();
  }

  ongoingexampagedata() {
    this._ongoingService.getPageData(this._userId, this._accessToken,this.searchText,this.sortBy, this.currentPageNumber)
    .subscribe((resp) => {
      this.pageData = resp.data;
      this.currentPageNumber = resp.page;
      this.dataPerPage = resp.data_per_page;
      this.totalData = resp.total;
      console.log(this.pageData);
    });
  }
  paginationNumberChanged(pageNum) {
    this.currentPageNumber = pageNum;
    this.ongoingexampagedata();
  }

  clearAllFilterations() {
    this.searchText = '';
    this.currentPageNumber = 1;
    this.ongoingexampagedata();
  }

  onSearchTextKeypress(evnt) {
    if(evnt.which == 13) {
      this.currentPageNumber = 1;
      this.ongoingexampagedata();
    }
  }
  getData(data:any) {
    console.log('data',data);
    this._router.navigateByUrl('/students/purchased-exam-details/'+data.puchased_paper_id)
  }

}
