import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-exam-report',
  templateUrl: './exam-report.component.html',
  styleUrls: ['./exam-report.component.scss']
})
export class ExamReportComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.initJquery();
  }

  initJquery() {
    setTimeout(() => {
      let gal = $('#QuestionCarousel');
      gal.owlCarousel({
        items:2,
        autoplay: false,
        loop:false,
        margin:0,
        rewind: true,
        lazyLoad:true,
        dots: false,
        nav: true,
        navText : ['<i class="icon icon-arrow-thin-left"></i>','<i class="icon icon-arrow-thin-right"></i>'],
        responsive:{
          0: {
            items: 1,
          },
          440: {
            items: 1,
          },
          500: {
            items: 1,
          },
          600: {
            items: 1,
          },
          767: {
            items: 1,
          },
          999: {
            items: 2,
          },
          1100: {
            items: 2,
          },
          1200: {
            items: 2
          }
        },
      });
    }, 500);
  }

}
