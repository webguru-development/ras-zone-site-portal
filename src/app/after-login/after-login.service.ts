import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService {

  private subject = new Subject<any>();

  constructor(private _http: HttpClient) { }

  setUserData(userData: any) {
    this.subject.next(userData);
  }

  getUserData(): Observable<any> {
    return this.subject.asObservable();
  }
  saveProfilePicData(userId, accessToken, profileImageData) {
    let fd = new FormData();
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);
    fd.append("profile_pic_data", JSON.stringify(profileImageData));

    return this._http.post(environment.apiBaseUrl + 'student-profile-pic-edit', fd)
      .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }


}

