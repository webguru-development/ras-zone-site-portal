import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProfileSettingsService {

  constructor(private _http: HttpClient) { }

  getPageData(userId, accessToken) {
    let fd = new FormData();
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);

    return this._http.post(environment.apiBaseUrl + 'student-profile-settings-page-data', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  editSetting(userId, accessToken, formData) {
    let fd = new FormData();
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);
    fd.append("user_title_id", formData.userTitle);
    fd.append("user_full_name", formData.userFullName);
    fd.append("contact_number", formData.phoneNumber);
    fd.append("address_street", '');
    fd.append("address_city", '');
    fd.append("address_state", '');
    fd.append("address_pin", '0');
    
    return this._http.post(environment.apiBaseUrl + 'student-profile-settings-edit', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
