import { Component, OnInit,EventEmitter } from '@angular/core';
import { FormBuilder,FormGroup, Validators} from '@angular/forms';
import { ProfileSettingsService } from './profile-settings.service';
import { Router } from '@angular/router';
import { AppVariables } from 'src/app/app.variables';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.scss']
})
export class ProfileSettingsComponent implements OnInit {

  private _userId: string = '';
  private _accessToken: string = '';
  public profileForm: FormGroup = this.formBuilder.group({
    'userTitle': ['', [Validators.required]],
    'userFullName': ['', [Validators.required, Validators.pattern(/^[A-Za-z ]+$/)]],
    'userEmail': ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'phoneNumber': ['', [Validators.required, Validators.pattern(/^[6-9]{1}[0-9]{9}$/)]]
  });
  public pageData: any = {};
  public submitBtnClicked: boolean = false;

  constructor(public formBuilder: FormBuilder, private _profileSettingsService: ProfileSettingsService, private _router: Router, public appVariables: AppVariables, private _toastr: ToastrService) { }

  ngOnInit(): void {
    try {
      let userDetails = JSON.parse(localStorage.getItem("user_data"));
      this._userId = userDetails.user_id;
      this._accessToken = userDetails.access_token;
      this.getPageData();
    } catch (error) {
      this._router.navigateByUrl('/paper-packages');
    }
  }

  getPageData() {
    this._profileSettingsService.getPageData(this._userId, this._accessToken)
    .subscribe(resp => {
      console.log(resp);
      if(resp.user_status == 401) {
        // this._emitter.emit('logOutUser', {
        //   userId: this._userId,
        //   accessToken: this._accessToken
        // });
        this._router.navigateByUrl('/paper-packages');
      }

      this.pageData = resp.data;      
      this.profileForm.controls['userTitle'].setValue(this.pageData.user_title_id);
      this.profileForm.controls['userFullName'].setValue(this.pageData.user_full_name);
      this.profileForm.controls['userEmail'].setValue(this.pageData.user_email);
      this.profileForm.controls['phoneNumber'].setValue(this.pageData.contact_number);
    });
  }

  profileFormSubmit() {

    if(!this.profileForm.controls['userFullName'].valid) {
      this._toastr.error(this.appVariables.profileSettingsFullNameError, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
    }
    if(!this.profileForm.controls['phoneNumber'].valid) {
      this._toastr.error(this.appVariables.profileSettingsContactNumberError, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
    }

    if(!!this.submitBtnClicked || this.profileForm.invalid) {
      return false;
    }
    
    this._profileSettingsService.editSetting(this._userId, this._accessToken, this.profileForm.value)
    .subscribe(resp => {
      console.log(resp.data);
      if(resp.user_status == 401) {
        // this._emitter.emit('logOutUser', {
        //   userId: this._userId,
        //   accessToken: this._accessToken
        // });
        this._router.navigateByUrl('/paper-packages');
      }
      if(resp.status == 200) {
        this._toastr.success(this.appVariables.profileSettingssuccess, '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
      }
      else {
        this._toastr.error(this.appVariables.someErrorOccurred, '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
      }
    });
  }

}
