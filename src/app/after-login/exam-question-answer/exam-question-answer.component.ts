import { Component, OnInit } from '@angular/core'; 
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, ParamMap  } from '@angular/router';
import {ExamQuestionAnswerService} from './exam-question-answer.service';
import { AppVariables } from 'src/app/app.variables';
import { HeaderService } from 'src/app/header/header.service';

@Component({
  selector: 'app-exam-question-answer',
  templateUrl: './exam-question-answer.component.html',
  styleUrls: ['./exam-question-answer.component.scss']
})
export class ExamQuestionAnswerComponent implements OnInit {

  public subject:any;
  public pageData: any = [];
  public _userId:any='';
  public _accessToken:any='';
  public topic_id: number = 8;
  public optionString:any=['A','B','C','D','E','F','G','H'];

  public currentQuestionIndex: number = 0;
  public questionAnswerListing: any = [];
  public startTimeInSec: number = 0;  
  public timerHrs: string = '00';
  public timerMins: string = '00';
  public timerSecs: string = '00';
  public isFinalSubmit :boolean = false;
  public paper_id: string = '';

  public isExamPaused: boolean = false;

  // answer_status = 0 --> Not Answered
  // answer_status = 1 --> Answered
  // answer_status = 2 --> Marked as review

  constructor(private _examQuestionAnswer: ExamQuestionAnswerService,private toastr: ToastrService,private _activatedRoute: ActivatedRoute,private _router: Router, private _headerService: HeaderService, public appVariables: AppVariables) { }

  ngOnInit(): void {
    let userDetails = JSON.parse(localStorage.getItem("user_data"));
    this._userId = userDetails.user_id;
    this._accessToken = userDetails.access_token;
    this.paper_id = this._activatedRoute.snapshot.params.paper_id;
    this.examQuestionsdata();
    this._examQuestionAnswer.currentData.subscribe(resp=>{
      let allData:any = resp.data;
      this.subject = allData[0];
      console.log(this.subject);
    })
  }

  examQuestionsdata() {
    this._examQuestionAnswer.getPageData(this._userId, this._accessToken, this.paper_id)
    .subscribe((resp) => {
      if(resp.status == 401) {
        this.toastr.error('Session timed out', '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
        
        this._headerService.logout(this._userId, this._accessToken)
        .subscribe(resp => {
          this._router.navigateByUrl('/paper-packages');
          window.localStorage.removeItem('user_data');
          this._userId = '';
          this._accessToken = '';
          this._headerService.setUserData(null);
        })
      }
      else if(resp.status == 400) {
        this.toastr.error('Invalid Parameters', '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });

        this._router.navigateByUrl('/students/purchased-exam-modules');
      }
      else if(resp.status == 200) {
        this.pageData = resp.data;
        this.questionAnswerListing = resp.data.question_listing;
        console.log(this.questionAnswerListing)
        this.startTimeInSec = this.pageData.remaining_time_in_seconds;
        this.timerFunction();
      }
    });
  }

  answerChecked(data) {
    console.log('checked')
    for(let obj of this.questionAnswerListing[this.currentQuestionIndex].question_details) {
      obj.check_status = false;
    }
    data.check_status = true;
    this.questionAnswerListing[this.currentQuestionIndex].answer_status = 1;
  }

  saveAnswerData(isFinalSubmit) {
    this._examQuestionAnswer.getAnswerSubmitData(this._userId,this._accessToken, this.pageData.exam_id, isFinalSubmit, this.questionAnswerListing).subscribe(resp =>{
      console.log(resp);
    })
  }

  nextQuestionClicked() {
    if(this.currentQuestionIndex < this.questionAnswerListing.length - 1) {
      this.currentQuestionIndex++;
    }
    console.log(this.questionAnswerListing);
    this.saveAnswerData(0);
  }

  prevQuestionClicked() {
    this.saveAnswerData(0);
    if(this.currentQuestionIndex > 0) {
      this.currentQuestionIndex--;
    }
  }

  clearOptionClicked() {
    for(let obj of this.questionAnswerListing[this.currentQuestionIndex].question_details) {
      obj.check_status = false;
    }
    this.questionAnswerListing[this.currentQuestionIndex].answer_status = 0;
    this.saveAnswerData(0);
  }

  markForReviewClicked() {
    if(this.questionAnswerListing[this.currentQuestionIndex].answer_status != 2) {
      this.questionAnswerListing[this.currentQuestionIndex].answer_status = 2;
    }
    else {
      let isOptionSelected = false;
      for(let obj of this.questionAnswerListing[this.currentQuestionIndex].question_details) {
        if(!!obj.check_status) {
          isOptionSelected = true;
          break;
        }
      }

      if(!isOptionSelected) {
        this.questionAnswerListing[this.currentQuestionIndex].answer_status = 0;
      }
      else {
        this.questionAnswerListing[this.currentQuestionIndex].answer_status = 1;
      }
    }
    this.questionAnswerListing[this.currentQuestionIndex].isFinalSubmit = false;
    this.saveAnswerData(0);
  }

  timerFunction() {
    this.timerHrs = ('0' + parseInt((this.startTimeInSec / 3600).toString())).slice(-2);
    this.timerMins = ('0' + parseInt(((this.startTimeInSec % 3600) / 60).toString())).slice(-2);
    this.timerSecs = ('0' + (this.startTimeInSec % 3600) % 60).slice(-2);

    if(!!this.isExamPaused) {
      return false;
    }

    setTimeout(() => {
      if(this.startTimeInSec == 300) {
        alert(this.appVariables.questionAnswerPage5MinsRemainingAlertMessage);
      }
    }, 10);
    
    setTimeout(() => {
      if(this.startTimeInSec == 0) {
        alert(this.appVariables.questionAnswerPageTimeUpAlertMessage);
        this.isFinalSubmit = true;
        return false;
      }

      this.startTimeInSec -= 1;
      this.timerFunction();
    }, 1000);
  }

  pauseClicked() {
    this.isExamPaused = !this.isExamPaused;
    if(!this.isExamPaused) { // resumed
      this.timerFunction();
      this.saveAnswerData(2);
    }
    else { //  paused
      this.saveAnswerData(1);
    }

  }

  answerSubmitClicked() {
    console.log(this.questionAnswerListing)
    this.saveAnswerData(4);
  }
}

