import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from "rxjs/operators";
import { Observable, Subject,BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExamQuestionAnswerService {

  public _subject = new BehaviorSubject({data:''});
  currentData = this._subject.asObservable();

  constructor(private _http: HttpClient) { }

  getPageData(userId, accessToken,paperId) {
    let fd = new FormData();
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);
    fd.append("paper_id", paperId);
    return this._http.post(environment.apiBaseUrl + 'student-exam-questions-page-data', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  getAnswerSubmitData(userId, accessToken,examId,isfinalsubmit,questionwiseanswerdata) {
    let fd = new FormData();
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);
    fd.append("exam_id", examId);
    fd.append("isfinalsubmit", isfinalsubmit);
    fd.append("questionwiseanswerdata", JSON.stringify(questionwiseanswerdata));
    return this._http.post(environment.apiBaseUrl + 'get-student-answer-submit-page-data', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  getPaperDeatils() {
    return this._subject.asObservable();
  }

  setName(data:any) {
    console.log('1',data);
    this._subject.next(data);
  }
}