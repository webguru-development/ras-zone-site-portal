import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PurchaseHistoryService {

  constructor(private _http: HttpClient) { }
  getPageData(userId, accessToken,searchText,sortBy, pageNo) {
    let fd = new FormData();
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);
    fd.append("search_text", searchText);
    fd.append("sort_by", sortBy);
    fd.append("page_no", pageNo);
    return this._http.post(environment.apiBaseUrl + 'get-purchase-history-page-data', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
