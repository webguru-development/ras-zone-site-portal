import { Component, OnInit } from '@angular/core';
import {PurchaseHistoryService} from './purchase-history.service'
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.scss']
})
export class PurchaseHistoryComponent implements OnInit {

  public user_data:any;
  public pageData: any = [];
  public sortBy: number = 5;
  public searchText: string = '';
  public tooltip: any =[];
  public blank: any = ' ';
  public _userId:any='';
  public _accessToken:any='';
  public currentPageNumber: number = 1;
  public dataPerPage: number = 0;
  public totalData: number = 0;

  private _activatedRouteQueryParamsSubscribe: any;
  constructor(private _purchasehistoryService: PurchaseHistoryService,private toastr: ToastrService,private _activatedRoute: ActivatedRoute,private _router: Router) {
    this._activatedRouteQueryParamsSubscribe = this._activatedRoute.queryParams.subscribe(queries => {
      if(typeof queries.search_text !== 'undefined' && queries.search_text != null) {
        this.searchText = queries.search_text;
      }
    })
   }

  ngOnInit(): void {
    let userDetails = JSON.parse(localStorage.getItem("user_data"));
    this._userId = userDetails.user_id;
    this._accessToken = userDetails.access_token;
    this.purchasehistorypageData();
  }

  purchasehistorypageData() {
    this._purchasehistoryService.getPageData(this._userId, this._accessToken,this.searchText,this.sortBy, this.currentPageNumber)
    .subscribe((resp) => {
      this.pageData = resp.data;
      this.currentPageNumber = resp.page;
      this.dataPerPage = resp.data_per_page;
      this.totalData = resp.total;
      console.log(this.pageData);

    });
  }
  paginationNumberChanged(pageNum) {
    this.currentPageNumber = pageNum;
    this.purchasehistorypageData();
  }

  clearAllFilterations() {
    this.searchText = '';
    this.currentPageNumber = 1;
    this.purchasehistorypageData();
  }

  onSearchTextKeypress(evnt) {
    if(evnt.which == 13) {
      this.currentPageNumber = 1;
      this.purchasehistorypageData();
    }
  }

  getTooltipText(paperList) {
    let str = '';
    for(let obj of paperList) {
      str += 'Paper Id: '+obj.paper_id+'\nPaper Name: ' + obj.paper_name +'\nPrice: ' + obj.paper_price +' INR\n\n';
    }
    return str;
  }

}
