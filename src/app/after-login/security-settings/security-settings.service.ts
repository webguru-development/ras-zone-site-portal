import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class SecuritySettingsService {

  constructor(private _http: HttpClient) { }

  changePassword(userId, accessToken, passwordFormData) {
    let fd = new FormData();
    
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);
    fd.append("old_password", passwordFormData.oldPassword.trim());
    fd.append("new_password", passwordFormData.newPassword.trim());
    fd.append("confirm_password", passwordFormData.confirmPassword.trim());

    return this._http.post(environment.apiBaseUrl + 'student-security-settings-edit', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
