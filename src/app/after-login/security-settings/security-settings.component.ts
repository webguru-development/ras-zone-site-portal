import { Component, OnInit } from '@angular/core';
import { EventEmitter } from 'events';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AppVariables } from '../../app.variables';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SecuritySettingsService } from './security-settings.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-security-settings',
  templateUrl: './security-settings.component.html',
  styleUrls: ['./security-settings.component.scss']
})
export class SecuritySettingsComponent implements OnInit {

  private _userId: string = '';
  private _accessToken: string = '';
  private _csrfToken: string = '';

  public showErrorOldPassword: boolean = false;
  public showErrorNewPassword: boolean = false;
  public showErrorConfirmNewPassword: boolean = false;
  public showErrorMisatchPassword: boolean = false;
  public submitBtnClicked: boolean = false;  

  public changePasswordFormGroup: FormGroup = this.formBuilder.group({
    'oldPassword': ['', [Validators.required]],
    'newPassword': ['', [Validators.required]],
    'confirmPassword': ['', [Validators.required]],
  });

  confirmPasswordValidation(control: FormControl) {
    if (typeof control.parent !== 'undefined') {
      if (control.value == '' || !(/^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*\_\-])[a-zA-Z0-9!@#$%^&*\_\-]{8,}$/.test(control.value))) {
        return { errorMessage: 'This field is mandatory' };
        // return control.parent.controls['password'].setErrors(null);
      }
      else if (control.parent.controls['newPassword'].value != control.parent.controls['confirmPassword'].value) {
        // console.log(1)
        return { errorMessage: 'This field is mandatory' };
        // return null;
      }else if (control.parent.controls['newPassword'].value == control.parent.controls['confirmPassword'].value) {
        // console.log(2)
        control.parent.controls['confirmPassword'].setErrors(null);
        control.parent.controls['newPassword'].setErrors(null);
        return null;
      }
      else {
        // console.log(3)
        control.parent.controls['confirmPassword'].setErrors(null);
        return null;
      }
    }
  }

  constructor(private _securitySettingsService: SecuritySettingsService, public formBuilder: FormBuilder, public appVariables: AppVariables, private _meta: Meta, private _title: Title,private toastr: ToastrService, private _router: Router) { }

  ngOnInit(): void {
    try {
      let userDetails = JSON.parse(localStorage.getItem("user_data"));
      this._userId = userDetails.user_id;
      this._accessToken = userDetails.access_token;      
    } catch (error) {
      this._router.navigateByUrl('/paper-packages');
    }
  }

  changePasswordFromValidation(evnt){  
    // Validation Starts
    let errValidation = false;
    this.showErrorOldPassword = false;
    this.showErrorNewPassword = false;

    if(!this.changePasswordFormGroup.controls['oldPassword'].valid || this.changePasswordFormGroup.value.oldPassword == '') {
      this.showErrorOldPassword = true;
      errValidation = true;
      this.toastr.info(this.appVariables.securitySettingsInvalidOldPasswordErrorMessage, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorOldPassword = false;      
    }
    
    if(!this.changePasswordFormGroup.controls['newPassword'].valid || this.changePasswordFormGroup.value.newPassword == '') {
      this.showErrorNewPassword = true;
      errValidation = true;
      this.toastr.info(this.appVariables.securitySettingsInvalidNewPasswordErrorMessage, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorNewPassword = false;      
    }

    if(!this.changePasswordFormGroup.controls['confirmPassword'].valid || this.changePasswordFormGroup.value.confirmPassword == '') {
      this.showErrorConfirmNewPassword = true;
      errValidation = true;
      this.toastr.info(this.appVariables.securitySettingsInvalidNewConfirmPasswordErrorMessage, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorConfirmNewPassword = false;      
    }

    if(this.changePasswordFormGroup.value.newPassword !== this.changePasswordFormGroup.value.confirmPassword) {
      this.showErrorMisatchPassword = true;
      errValidation = true;
      this.toastr.info(this.appVariables.securitySettingsInvalidMismatchPasswordErrorMessage, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorMisatchPassword = false;      
    }

    console.log(errValidation)
    if(!!errValidation) {      
      // setTimeout(() => {
      //   $('html, body').animate({'scrollTop': $('.form-group small.text-danger').eq(0).offset().top - 150}, 300);
      // }, 20);
      evnt.stopPropagation();
      return false;
    }
    else{
      return true;
    }
  }

  changePasswordFormSubmit(){
    console.log(this.changePasswordFormGroup.value);
    this.submitBtnClicked = true;
    this._securitySettingsService.changePassword(this._userId,this._accessToken,this.changePasswordFormGroup.value)
      .subscribe((resp) => {
        this.submitBtnClicked = false;
        console.log(resp);
        if (resp.status == 400) {
          // this.showErrorDuplicateUserEmail = true;
          this.toastr.error(this.appVariables.securitySettingsOldPasswordErrorRespMessage, '', {
            timeOut: 3000,
            positionClass:'toast-bottom-right'
          });
          // return false;
        }        
        else if (resp.status == 200) {
          // this.showMessage = true;
          // this.message = this.appVariables.createsuccess;
          this.toastr.success(this.appVariables.securitySettingsPasswordChangedSuccessRespMessage, '', {
            timeOut: 3000,
            positionClass:'toast-bottom-right'
          });
          this.changePasswordFormGroup.reset();          
        }
        else {
          // this.showMessage = true;
          // this.message = this.appVariables.someErrorOccurred;
        }
      }, err => {
        console.log(err)
        this.toastr.error(this.appVariables.someErrorOccurred, '', {
          timeOut: 3000,
          positionClass:'toast-bottom-right'
        });

        // if(err.status == 401) {
        //   // this._router.navigateByUrl('/login');
        // }
        // else if(err.status == 400){
          
        // }
        // else{
          
        // }
      });
  }  

}
