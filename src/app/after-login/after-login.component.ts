import { Component, OnInit,TemplateRef, EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AppVariables } from '../app.variables';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
// import { EventEmitter } from 'events';
import { HeaderService } from '../header/header.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {AfterLoginService} from './after-login.service'

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.scss']
})
export class AfterLoginComponent implements OnInit {

  modalRef: BsModalRef;
  public userId: string = '';
  public userName: string = '';
  public accessToken: string = '';
  public successMessage=false;
  public profilePicUrlJpeg: string = '';
  public profilePicImageChangedEvent: string = '';
  public editImage: boolean = false;
  public editImageIndex: number = -1;
  public sliderImageChangedEvent: string = '';
  public profileImageData = {
    source_img: '',
    cropped_img: '',
    crop_data: {
      x: 0.1,
      y: 0.1,
      w: 0.8,
      h: 0.8
    }
  };
  public profilePicUrl: string = '';
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  public imageSavingNow: boolean = false;

  // public createEditGymForm: FormGroup = this.formBuilder.group({
  //   'profilePicImageCroppedResult': [this.profilePicImageCroppedResult, []]
  // });
  constructor(private _headerService: HeaderService, private _router: Router, private _toastr: ToastrService, private _appVariables: AppVariables,private formBuilder:FormBuilder,private _modalService: BsModalService,public _afterloginService:AfterLoginService) { }

  ngOnInit(): void {
    let userData = window.localStorage.getItem('user_data');
    try {
      this.userId = JSON.parse(userData).user_id;
      this.userName = JSON.parse(userData).user_name;
      this.accessToken = JSON.parse(userData).access_token;
      /* this.profilePicUrlJpeg = this._appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 'c-profile-pic.jpeg?t=' + (new Date().getTime());*/
      this.profilePicUrlJpeg = this._appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 'c-profile-pic.jpeg';
    } catch (error) {
      this._router.navigateByUrl('/login');
    }
  }

  logoutClicked() {
    this._headerService.logout(this.userId, this.accessToken)
    .subscribe(resp => {
      this._router.navigateByUrl('/paper-packages');
      window.localStorage.removeItem('user_data');
      this.userId = '';
      this.accessToken = '';
      this._headerService.setUserData(null);
      this._toastr.success(this._appVariables.logoutSuccessMessage, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
    })
  }

  getDataUri(sourceImageFullPath, croppedImageFullPath = '') {
    return new Promise((resolve, reject) => {
      let canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d"),
        img = new Image(),
        data = {
          croppedImageUrl: '',
          sourceImageUrl: ''
        },
        imgType = 'source';

      img.crossOrigin = "anonymous";
      img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);
        if (imgType == 'source') {
          
          data.sourceImageUrl = canvas.toDataURL('image/'+sourceImageFullPath.split('.')[sourceImageFullPath.split('.').length - 1]);
          if (croppedImageFullPath != '') {
            imgType = 'crop';
            img.src = croppedImageFullPath;
          }
          else {
            resolve(data);
          }
        }
        else if (imgType == 'crop') {
          imgType = '';
          data.croppedImageUrl = canvas.toDataURL('image/'+croppedImageFullPath.split('.')[croppedImageFullPath.split('.').length - 1]);
          resolve(data);
        }
      };
      img.src = sourceImageFullPath;
    });
  }

  imageChangePopup(template: TemplateRef<any>) {
    this.modalRef = this._modalService.show(template, this.config);
    setTimeout(() => {
      this.getDataUri(this._appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId +'s-profile-pic.jpeg?t=' + (new Date().getTime())).then((res) => {        
        this.profileImageData.source_img = res['sourceImageUrl'],
        this.profileImageData.cropped_img = res['sourceImageUrl'],
        setTimeout(() => {
          this.profilePicImageChangedEvent = res['sourceImageUrl'].toString();
        }, 200);
      });
    }, 500);
  }

  
  profilePicFileUpload(evnt) {
    let _this = evnt.target;
    if (_this.files && _this.files[0]) {
      let fr = new FileReader();

      fr.onload = (evt) => {
        this.profilePicImageChangedEvent = evt.target['result'].toString();
      }
      fr.readAsDataURL(_this.files[0]);
    }
  }

  profilePicCroppedOutput(croppedResult) {
    this.profileImageData = {
      source_img: croppedResult.sourceImage,
      cropped_img: croppedResult.croppedImage,
      crop_data: {
        x: croppedResult.croppedData.x,
        y: croppedResult.croppedData.y,
        w: croppedResult.croppedData.w,
        h: croppedResult.croppedData.h
      }
    };
  }
  saveProfilePicDataFunc() {

    // this._emitter.emit('showLoader');
    this.imageSavingNow = true;
    this._afterloginService.saveProfilePicData(this.userId, this.accessToken,this.profileImageData)
    .subscribe(resp => {
      this.imageSavingNow = false;
      // this._emitter.emit('hideLoader');
      if(resp.user_status == 401) {
        // this._emitter.emit('logOutUser', {
        //   userId: this.userId,
        //   accessToken: this.accessToken
        // });
        this._router.navigateByUrl('/index');
      }
      else if(resp.status == 200) {
        this.successMessage = true;
        this.profileImageData = {
          source_img: this._appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 's-profile-pic.jpeg?t=' + (new Date().getTime()),
          cropped_img: this._appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 'c-profile-pic.jpeg?t=' + (new Date().getTime()),
          crop_data: {
            x: this.profileImageData.crop_data.x != null ? this.profileImageData.crop_data.x : 0.1,
            y: this.profileImageData.crop_data.y != null ? this.profileImageData.crop_data.y : 0.1,
            w: this.profileImageData.crop_data.w != null ? this.profileImageData.crop_data.w : 0.8,
            h: this.profileImageData.crop_data.h != null ? this.profileImageData.crop_data.h : 0.8
          }
        };
        
        this.profilePicUrlJpeg = this._appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 'c-profile-pic.jpeg?t=' + (new Date().getTime());

        this._headerService.setUserData({
          user_id: this.userId,
          user_name: this.userName,
          access_token: this.accessToken
        });

        setTimeout(() => {
          this.modalRef.hide();
        }, 2000);
      }
    }, (err) => {
      this.imageSavingNow = false;
    });

  }

}
