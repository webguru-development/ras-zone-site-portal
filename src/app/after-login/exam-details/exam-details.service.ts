import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ExamDetailsService {

  constructor(private _http: HttpClient) { }
  getPageData(userId, accessToken,paperId) {
    let fd = new FormData();
    fd.append("user_id", userId);
    fd.append("access_token", accessToken);
    fd.append("paper_id", paperId);
    return this._http.post(environment.apiBaseUrl + 'student-exam-details-page-data', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
