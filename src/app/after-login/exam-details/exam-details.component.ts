import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {ExamDetailsService} from './exam-details.service';
import { Router, ActivatedRoute, ParamMap  } from '@angular/router';
import {ExamQuestionAnswerService} from '../exam-question-answer/exam-question-answer.service';

@Component({
  selector: 'app-exam-details',
  templateUrl: './exam-details.component.html',
  styleUrls: ['./exam-details.component.scss']
})
export class ExamDetailsComponent implements OnInit {

  public user_data:any;
  public paper_data:any = {}
  public paper_id:any='';
  public pageData: any = [];
  public _userId:any='';
  public _accessToken:any='';

  constructor(private _examDetailsService: ExamDetailsService,private toastr: ToastrService,private _activatedRoute: ActivatedRoute,private _router: Router,private _examQuestion:ExamQuestionAnswerService) { }

  ngOnInit(): void {
    let userDetails = JSON.parse(localStorage.getItem("user_data"));
    this._userId = userDetails.user_id;
    this._accessToken = userDetails.access_token;
    this.paper_id = this._activatedRoute.snapshot.params.paper_id;
    this.examDetailsdata();
  }

  examDetailsdata() {
    this._examDetailsService.getPageData(this._userId, this._accessToken,this.paper_id)
    .subscribe((resp) => {
      if(resp.status == 400) {
        this._router.navigate(['/students/purchased-exam-modules']);
      } else {
      this.pageData = resp.data.papers_listing[0];
      this.paper_data["paper_name"] = resp.data.papers_listing[0].paper_name;
      this.paper_data["duration"] = resp.data.papers_listing[0].duration_in_minutes;
      this.paper_data["total_marks"] = resp.data.papers_listing[0].total_score;
      this.pageData.paper_details[0]['paper_id'] = this.pageData.paper_id
      console.log(this.pageData);
      }
    });
  }

  getAttemptdata() {
    this._router.navigateByUrl('/students/purchased-exam-questions/'+this.paper_id);
  }
  

}
