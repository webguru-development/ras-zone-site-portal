import { Injectable } from '@angular/core';

@Injectable()
export class AppConfigs {
  // Local Dev Server
  apiBaseUrl: string = 'https://localhost:4310/site-api/';
  baseUrl: string = 'https://localhost:4310';
  
  // // AWS Dev Server
  // apiBaseUrl: string = 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/p2/events_partner/public/api/';
  // baseUrl: string = 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com:8000/';
  // adminProfileBaseUrl: string = 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/events_partner/test-portal/';

  // // AWS Beta Server
  // apiBaseUrl: string = '';
  // baseUrl: string = '';
  // adminProfileBaseUrl: string = '';
  // sitePortalBaseUrl: string = '';
  

  // // AWS Live Server
  // apiBaseUrl: string = 'https://www.test.com:8443/site-portal-api/';
  // baseUrl: string = 'https://www.test.com:8443/';  
  // sitePortalBaseUrl: string = 'https://www.test.com/';

}