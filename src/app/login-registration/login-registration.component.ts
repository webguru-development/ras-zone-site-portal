import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { LoginRegistrationService } from './login-registration.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AppVariables } from '../app.variables';
declare var $: any;
import { ToastrService } from 'ngx-toastr';
import { HeaderService } from '../header/header.service'

@Component({
  selector: 'app-login-registration',
  templateUrl: './login-registration.component.html',
  styleUrls: ['./login-registration.component.scss']
})
export class LoginRegistrationComponent implements OnInit {

  constructor(private _loginRegistrationService: LoginRegistrationService, private _headerService: HeaderService, public appVariables: AppVariables, public formBuilder: FormBuilder, private toastr: ToastrService, private _router: Router) { }

  public pageData: any = {};
  
  public userTitleListings: any = [];
  public countryListings: any = [];
  public stateListings: any = [];
  public cityListings: any = [];
  
  public showErrorUserTitle: boolean = false;
  public showErrorUserName: boolean = false;
  public showErrorUserEmail: boolean = false;
  public showErrorUserPhoneNumber: boolean = false;
  public showErrorUserPassword: boolean = false;
  public showErrorUserConfirmPassword: boolean = false;
  public showErrorStreet: boolean = false;
  public showErrorCountry: boolean = false;
  public showErrorState: boolean = false;
  public showErrorCity: boolean = false;
  public showErrorPin: boolean = false;
  public showErrorTermsConditions: boolean = false;
  public ragistrationBtnClicked: boolean = false;
  public showErrorDuplicateUserEmail: boolean = false;
  
  public showErrorLoginUserEmail: boolean = false;
  public showErrorLoginUserPassword: boolean = false;
  public loginBtnClicked: boolean = false;
  

  public registrationFormGroup: FormGroup = this.formBuilder.group({
    'userTitle': ['', [Validators.required]],
    'userName': ['', [Validators.required, Validators.pattern(/^[^-\s][A-Za-z ]+$/)]],
    'userEmail': ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'phoneNumber': ['', [Validators.required, Validators.pattern(/^[6-9]{1}[0-9]{9}/)]],
    'password': ['', [this.confirmPasswordValidation, Validators.minLength(8)]],
    'confirmPassword': ['', [this.confirmPasswordValidation]],        
    'street': ['', []],
    'country': ['', []],
    'state': ['', []],
    'city': ['', []],
    'pin': ['0', []],
    'termsConditions': [false, []],
  });

  public loginFormGroup: FormGroup = this.formBuilder.group({
    'userEmail': ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'userPassword': ['', [Validators.required]],
  });

  confirmPasswordValidation(control: FormControl) {
    if (typeof control.parent !== 'undefined') {
      if (control.value == '' || !(/^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*\_\-])[a-zA-Z0-9!@#$%^&*\_\-]{8,}$/.test(control.value))) {
        return { errorMessage: 'This field is mandatory' };
        // return control.parent.controls['password'].setErrors(null);
      }
      else if (control.parent.controls['password'].value != control.parent.controls['confirmPassword'].value) {
        // console.log(1)
        return { errorMessage: 'This field is mandatory' };
        // return null;
      }else if (control.parent.controls['password'].value == control.parent.controls['confirmPassword'].value) {
        // console.log(2)
        control.parent.controls['confirmPassword'].setErrors(null);
        control.parent.controls['password'].setErrors(null);
        return null;
      }
      else {
        // console.log(3)
        control.parent.controls['confirmPassword'].setErrors(null);
        return null;
      }
    }
  }

  ngOnInit(): void {
    this.getPageDataFunc();
  }

  loginFromValidation(evnt){  
    // Validation Starts
    let errValidation = false;
    this.showErrorLoginUserEmail = false;
    this.showErrorLoginUserPassword = false;

    if(!this.loginFormGroup.controls['userEmail'].valid || this.loginFormGroup.value.userEmail == '') {
      this.showErrorLoginUserEmail = true;
      errValidation = true;
      this.toastr.info(this.appVariables.loginEmailValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorLoginUserEmail = false;      
    }
    
    if(!this.loginFormGroup.controls['userPassword'].valid || this.loginFormGroup.value.userPassword == '') {
      this.showErrorLoginUserPassword = true;
      errValidation = true;
      this.toastr.info(this.appVariables.loginPasswordValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorLoginUserPassword = false;      
    }    
    console.log(errValidation)
    if(!!errValidation) {      
      // setTimeout(() => {
      //   $('html, body').animate({'scrollTop': $('.form-group small.text-danger').eq(0).offset().top - 150}, 300);
      // }, 20);
      evnt.stopPropagation();
      return false;
    }
    else{
      return true;
    }
  }

  loginFormSubmit(){    
    this.loginBtnClicked = true;
    this._loginRegistrationService.studentLogin(this.loginFormGroup.value)
    .subscribe((resp) => {
      this.loginBtnClicked = false;        
      if (resp.status == 204) {
        this.toastr.error(this.appVariables.invalidEmailOrPasswordMessage, '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
      }                        
      else if (resp.status == 200) {
        window.localStorage.setItem('user_data', JSON.stringify(resp.data));          
        this.loginFormGroup.reset(); 
        this.toastr.success(this.appVariables.loginSuccessMessage, '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
        this._headerService.setUserData(resp.data);
        this._router.navigateByUrl('/paper-packages');
      }
      else {

      }
    }, err => {
      this.toastr.error(this.appVariables.someErrorOccurred, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    });
  }  

  registrationFromValidation(evnt){  
    // Validation Starts
    let errValidation = false;
    this.showErrorUserTitle = false;
    this.showErrorUserName = false;
    this.showErrorUserEmail = false;
    this.showErrorUserPhoneNumber = false;
    this.showErrorCountry = false;
    this.showErrorStreet = false;
    this.showErrorState = false;
    this.showErrorCity = false;
    this.showErrorPin = false;
    this.showErrorUserPassword = false;
    this.showErrorUserConfirmPassword = false;
    this.showErrorTermsConditions = false;
    this.ragistrationBtnClicked = false;
    this.showErrorDuplicateUserEmail = false;
      
  
    if(!this.registrationFormGroup.controls['userTitle'].valid || this.registrationFormGroup.value.userTitle == '') {
      this.showErrorUserTitle = true;      
      errValidation = true;
      this.toastr.info(this.appVariables.createTitleValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorUserTitle = false;
    }
    if(!this.registrationFormGroup.controls['userEmail'].valid || this.registrationFormGroup.value.userEmail == '') {
      this.showErrorUserEmail = true;      
      errValidation = true;
      this.toastr.info(this.appVariables.createEmailValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorUserEmail = false;
    }
    if(!this.registrationFormGroup.controls['userName'].valid || this.registrationFormGroup.value.userName == '') {
      this.showErrorUserName = true;
      errValidation = true;
      this.toastr.info(this.appVariables.createFullNameValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorUserName = false;
    }
    if(!this.registrationFormGroup.controls['phoneNumber'].valid || this.registrationFormGroup.value.phoneNumber == '') {
      this.showErrorUserPhoneNumber = true;
      errValidation = true;
      this.toastr.info(this.appVariables.createPhoneNumberValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorUserPhoneNumber = false;
    }
    if(this.registrationFormGroup.value.password == '') {
      this.showErrorUserPassword = true;
      errValidation = true;
      this.toastr.info(this.appVariables.createPasswordValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorUserPassword = false;
    }
    if(this.registrationFormGroup.value.password != this.registrationFormGroup.value.confirmPassword) {
      this.showErrorUserConfirmPassword = true;
      errValidation = true;
      this.toastr.info(this.appVariables.createPasswordMismatchValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
    else{
      this.showErrorUserConfirmPassword = false;
    }
    if(this.registrationFormGroup.controls['confirmPassword'].invalid || this.registrationFormGroup.value.confirmPassword == '') {
      this.showErrorUserConfirmPassword = true;
      errValidation = true;
    }
    else {
      this.showErrorUserConfirmPassword = false;
    }
    if(this.registrationFormGroup.controls['termsConditions'].value == false) {
      this.showErrorTermsConditions = true;
      this.toastr.info(this.appVariables.createTermsConditionValidationError, '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
      errValidation = true;
    }
    else {
      this.showErrorTermsConditions = false;      
    }  
    if(!!errValidation) {
      console.log(this.registrationFormGroup.valid)
      // setTimeout(() => {
      //   $('html, body').animate({'scrollTop': $('.form-group small.text-danger').eq(0).offset().top - 150}, 300);
      // }, 20);
      evnt.stopPropagation();
      return false;
    }
    else{
      return true;
    }
  }

  registrationFormSubmit(){
    // console.log(this.registrationFormGroup.value);
    this.ragistrationBtnClicked = true;
    this._loginRegistrationService.studentRegistration(this.registrationFormGroup.value)
      .subscribe((resp) => {
        this.ragistrationBtnClicked = false;
        console.log(resp);
        if (resp.status == 409) {
          this.showErrorDuplicateUserEmail = true;
          // this.toastr.warning(this.appVariables.createDuplicateEmailError);
          this.toastr.error(this.appVariables.createDuplicateEmailError, '', {
            timeOut: 3000,
            positionClass:'toast-bottom-right'
          });
          return false;
        }        
        else if (resp.status == 200) {
          // this.showMessage = true;
          // this.message = this.appVariables.createsuccess;
          this.registrationFormGroup.reset();
          this._router.navigateByUrl('/student-registration-success');
        }
        else {
          // this.showMessage = true;
          // this.message = this.appVariables.someErrorOccurred;
        }
      }, err => {
        console.log(err)
        this.toastr.error(this.appVariables.someErrorOccurred, '', {
          timeOut: 3000,
          positionClass:'toast-bottom-right'
        });

        // if(err.status == 401) {
        //   // this._router.navigateByUrl('/login');
        // }
        // else if(err.status == 400){
          
        // }
        // else{
          
        // }
      });
  }

  getPageDataFunc(){
    // this._emitter.emit('showLoader');
    this._loginRegistrationService.getPageData()
    .subscribe((resp) => {
      this.pageData = resp.data;
      this.userTitleListings = resp.data.user_titles_listing;
      this.countryListings = resp.data.countries_listing;
      // console.log(resp);
      if(resp.user_status == 401) {
        // this._emitter.emit('logOutUser', {
        //   userId: this._userId,
        //   accessToken: this._accessToken
        // });
      }
      if(resp.status == 200) {
        // this.pageData = resp;
      }
      else {
        // this._emitter.emit('hideLoader');
      }
    }, err => {
    });
  }

  onStateChanged() {
    this.registrationFormGroup.controls['city'].setValue('');
    
    this._loginRegistrationService.getCityByState(this.registrationFormGroup.value.state)
    .subscribe((resp) => {
      console.log(resp);

      if(resp.status == 401) {
        
      }
      else if(resp.status == 200) {
        this.cityListings = resp.data;
        if(resp.data != ''){
          this.registrationFormGroup.controls['city'].setValue(this.cityListings[0].city_id);
        }        
      }
      else {        
      }
    }, err => {
    });
  }

  onCountryChanged() {
    this.registrationFormGroup.controls['state'].setValue('');
    this.registrationFormGroup.controls['city'].setValue('');
    // this._emitter.emit('showLoader');
    this._loginRegistrationService.getStateByCountry(this.registrationFormGroup.value.country)
    .subscribe((resp) => {
      if(resp.status == 401) {
        // this._router.navigateByUrl('/login');
      }
      else if(resp.status == 200) {
        this.stateListings = resp.data;
        this.registrationFormGroup.controls['state'].setValue(this.stateListings[0].state_id);
      }
      else {
        
      }
    }, err => {
      console.log(err)
    });    
  }

}
