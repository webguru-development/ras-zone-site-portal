import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class LoginRegistrationService {

  constructor(private _http: HttpClient) { }

  studentRegistration(formData) {
    let fd = new FormData();
    fd.append("user_title_id", formData.userTitle);
    fd.append("user_full_name", formData.userName);
    fd.append("user_email", formData.userEmail);
    fd.append("user_password", formData.password);
    fd.append("user_confirm_password", formData.confirmPassword);
    fd.append("contact_number", formData.phoneNumber);    
    fd.append("address_street", formData.street);
    fd.append("address_city", formData.city);
    fd.append("address_state", formData.state);
    fd.append("address_country", formData.country);
    fd.append("address_pin", formData.pin);

    return this._http.post(environment.apiBaseUrl + 'student-registration', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  studentLogin(formData) {
    let fd = new FormData();
    fd.append("user_email", formData.userEmail);
    fd.append("user_password", formData.userPassword);

    return this._http.post(environment.apiBaseUrl+'student-login', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  getPageData() {
    let fd = new FormData();
    return this._http.post(environment.apiBaseUrl + 'student-registration-page-data', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  getCityByState(state) {
    let fd = new FormData();
    fd.append("state_id", state);

    return this._http.post(environment.apiBaseUrl + 'get-city-by-state', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  getStateByCountry(country) {
    let fd = new FormData();
    fd.append("country_id", country);

    return this._http.post(environment.apiBaseUrl + 'get-state-by-country', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  
}
