import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaperPackagesComponent } from './paper-packages/paper-packages.component';
import { LoginRegistrationComponent } from './login-registration/login-registration.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { StudentRegistrationActivateAccountComponent } from './student-registration-activate-account/student-registration-activate-account.component';
import { StudentRegistrationSuccessComponent } from './student-registration-success/student-registration-success.component';
import { StudentResetPasswordComponent } from './student-reset-password/student-reset-password.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AfterLoginComponent } from './after-login/after-login.component';
import { ProfileSettingsComponent } from './after-login/profile-settings/profile-settings.component';
import { SecuritySettingsComponent } from './after-login/security-settings/security-settings.component';
import { PurchaseHistoryComponent } from './after-login/purchase-history/purchase-history.component';
import {ExamModuleComponent} from './after-login/exam-module/exam-module.component'
import { ExamDetailsComponent } from './after-login/exam-details/exam-details.component';
import { ExamQuestionAnswerComponent } from './after-login/exam-question-answer/exam-question-answer.component';
import { ExamReportComponent } from './after-login/exam-report/exam-report.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/paper-packages' },
  { path: 'paper-packages', component: PaperPackagesComponent },
  { path: 'login', component: LoginRegistrationComponent },
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'student-registration-success', component:StudentRegistrationSuccessComponent},  
  { path: 'account-activate/:activationToken', component: StudentRegistrationActivateAccountComponent },  
  { path: 'student-reset-password/:resetToken', component: StudentResetPasswordComponent },
  {
    path: 'students',
    component: AfterLoginComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: '/students/profile-settings' },
      { path: 'profile-settings', component: ProfileSettingsComponent},
      { path: 'security-settings', component: SecuritySettingsComponent},
      { path: 'purchase-history', component: PurchaseHistoryComponent},
      { path: 'purchased-exam-modules', component: ExamModuleComponent},
      { path: 'purchased-exam-details/:paper_id', component: ExamDetailsComponent},
      { path: 'purchased-exam-questions/:paper_id', component: ExamQuestionAnswerComponent},
      { path: 'purchased-exam-reports', component: ExamReportComponent},
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
