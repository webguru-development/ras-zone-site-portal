import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentResetPasswordService } from './student-reset-password.service';
import { AppVariables } from '../app.variables';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-student-reset-password',
  templateUrl: './student-reset-password.component.html',
  styleUrls: ['./student-reset-password.component.scss']
})
export class StudentResetPasswordComponent implements OnInit {

  public passwordErrorShow: boolean = false;
  public confirmPasswordErrorShow: boolean = false;
  public resetToken: string = '';

  public resetForm: FormGroup = this._formBuilder.group({
    'password': ['', [this.passwordMatchValidation]],
    'confirmPassword': ['', [this.confirmPasswordMatchValidation]]
  });

  passwordMatchValidation(control: FormControl) {
    if (typeof control.parent !== 'undefined') {
      if (control.value == '' || !(/^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*\_\-])[a-zA-Z0-9!@#$%^&*\_\-]{8,}$/.test(control.value))) {
        return { errorMessage: 'This field is mandatory' };
      }
      else if (control.parent.controls['password'].value != control.parent.controls['confirmPassword'].value) {
        control.parent.controls['password'].setErrors({ 'incorrect': true });
        return null;
      }
      else {
        control.parent.controls['confirmPassword'].setErrors(null);
        return null;
      }
    }
  }

  confirmPasswordMatchValidation(control: FormControl) {
    if (typeof control.parent !== 'undefined') {
      if (control.value == '') {
        return { errorMessage: 'This field is mandatory' };
      }
      else if (control.parent.controls['password'].value != control.parent.controls['confirmPassword'].value) {
        return { errorMessage: 'This field is mandatory' };
      }
      else {
        control.parent.controls['confirmPassword'].setErrors(null);
        return null;
      }
    }
  }

  constructor(private _activatedRoute: ActivatedRoute, private _studentResetPasswordService: StudentResetPasswordService, private _router: Router, public appVariables: AppVariables, private _formBuilder: FormBuilder, private _toastr: ToastrService) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((param) => {
      if(typeof param.resetToken === 'undefined' || param.resetToken == '' || param.resetToken == null) {
        this._router.navigateByUrl('/404', {skipLocationChange: true});
      }
      else {
        this.resetToken = param.resetToken;
        this._studentResetPasswordService.pageValidate(this.resetToken)
        .subscribe((resp) => {
          // console.log(resp);
          if(resp.status == 200) {
          }
          else {
            this._router.navigateByUrl('/404', {skipLocationChange: true});
          }
        }, (error) => {
          this._router.navigateByUrl('/404', {skipLocationChange: true});
        });
      }
    });
  }

  resetClicked() {
    if(!this.resetForm.controls['password'].valid) {
      this._toastr.error(this.appVariables.resetPasswordPasswordError, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
      return false;
    }
    else if(!this.resetForm.controls['confirmPassword'].valid) {
      this._toastr.error(this.appVariables.resetPasswordPasswordMismatchedError, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
      return false;
    }
    
    if(!this.resetForm.valid) {
      return false;
    }

    this._studentResetPasswordService.resetPassword(this.resetToken, this.resetForm.value)
    .subscribe((resp) => {
      // console.log(resp);
      if(resp.status == 200) {
        this._toastr.success(this.appVariables.resetPasswordsuccess, '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
        setTimeout(() => {
          this._router.navigateByUrl('/login');
        }, 3000);
      }
      else {
        this._toastr.error(this.appVariables.someErrorOccurred, '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
      }
    }, (error) => {
      this._toastr.error(this.appVariables.someErrorOccurred, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
    });
  }

}
