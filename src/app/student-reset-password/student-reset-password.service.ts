import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class StudentResetPasswordService {

  constructor(private _http: HttpClient) { }

  pageValidate(requestToken) {
    let fd = new FormData();
    fd.append("request_token", requestToken);

    return this._http.post(environment.apiBaseUrl + 'student-reset-password-auth-check', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  resetPassword(requestToken, formData) {
    let fd = new FormData();
    fd.append("request_token", requestToken);
    fd.append("new_password", formData.password);
    fd.append("confirm_password", formData.confirmPassword);

    return this._http.post(environment.apiBaseUrl + 'student-reset-password', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
