import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PaperPackagesService {

  constructor(private _http: HttpClient) { }

  getPageData() {
    let fd = new FormData();
    return this._http.post(environment.apiBaseUrl + 'get-paper-packages-page-data', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  submitPurchase(user_data,purchase_packages,is_all){
    let fd = new FormData();
    fd.append("user_id", user_data.user_id);
    fd.append("access_token", user_data.access_token);
    fd.append("purchase_packages", JSON.stringify(purchase_packages));
    fd.append("is_all_packages", is_all);
    return this._http.post(environment.apiBaseUrl + 'student-purchase-paper', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
