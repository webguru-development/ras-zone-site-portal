import { Component, OnInit } from '@angular/core';
import { PaperPackagesService } from './paper-packages.service';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-paper-packages',
  templateUrl: './paper-packages.component.html',
  styleUrls: ['./paper-packages.component.scss']
})

export class PaperPackagesComponent implements OnInit {

  public pageData: any = {};
  public papersListing: any = [];
  public popupData: any = {};
  public selectAllPaperRadioBtn: boolean = false;
  public paperSelected: number = 0;
  public allPackagesTotalPrice: number = 0;
  public allPackagesDiscountPrice: number = 0;

  public packagesArr:any = [];
  public user_data:any;

  constructor(private _paperPackagesService: PaperPackagesService,private toastr: ToastrService) { }

  
  ngOnInit(): void {

    this.user_data =  window.localStorage.getItem('user_data');
    this.user_data = JSON.parse(this.user_data);
    this.paperPackageData();
  }

  paperPackageData() {
    this._paperPackagesService.getPageData()
    .subscribe((resp) => {
      console.log(resp);
      this.pageData = resp;
      this.papersListing = resp.data.papers_listing;

      for(let obj of this.papersListing) {
        this.allPackagesTotalPrice += parseInt(obj.paper_price);
      }

      this.allPackagesDiscountPrice = this.allPackagesTotalPrice - parseInt((this.allPackagesTotalPrice * resp.data.discount_percentage_all_packages / 100).toString());
    });
  }

  detailsPopupClicked(popupData) {
    this.popupData = popupData;
  }

  selectAllRadioBtnClicked() {
    this.selectAllPaperRadioBtn = !this.selectAllPaperRadioBtn;

    for(let obj of this.papersListing) {
      obj.paper_checked = this.selectAllPaperRadioBtn;
    }

    this.paperSelected = this.papersListing.length;
  }

  
  paperCheckBoxClicked(clickedData) {
    let packagesArr = [];
    this.packagesArr.push({ 'paper_id' : clickedData.paper_id });
    console.log(this.packagesArr);
    
    let allCheckedFlag = true;
    this.paperSelected = 0;
    for(let obj of this.papersListing) {
      if(obj.paper_id == clickedData.paper_id) {
        obj.paper_checked = !obj.paper_checked;
      }

      if(!obj.paper_checked) {
        allCheckedFlag = false;
      }
      else {
        this.paperSelected++;
      }
    }

    this.selectAllPaperRadioBtn = allCheckedFlag;
  }

  

  purchaseSection(){
    if(this.user_data){
      this._paperPackagesService.submitPurchase(this.user_data,this.packagesArr,false).subscribe(result=>{
        this.toastr.info('You have purchase this packages', '', {
          timeOut: 3000,
          positionClass:'toast-bottom-right'
        });
      });
    }else{
      this.toastr.info('Please login to buy a package', '', {
        timeOut: 3000,
        positionClass:'toast-bottom-right'
      });
    }
  }

  onSubmit(){
    this.purchaseSection();
  }
}
