import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { AppConfigs } from './app.configs';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule  } from 'ngx-bootstrap/modal';
import { NgxMsdImageCropperComponent } from './ngx-msd-image-editor/msd-image-cropper.component';
import {NgxMsdPaginationComponent} from './ngx-msd-pagination/ngx-msd-pagination.component'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PaperPackagesComponent } from './paper-packages/paper-packages.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginRegistrationComponent } from './login-registration/login-registration.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppVariables } from './app.variables';
import { StudentRegistrationActivateAccountComponent } from './student-registration-activate-account/student-registration-activate-account.component';
import { StudentRegistrationSuccessComponent } from './student-registration-success/student-registration-success.component';
import { StudentResetPasswordComponent } from './student-reset-password/student-reset-password.component';
import { PaperPackagesService } from './paper-packages/paper-packages.service';
import { ProfileSettingsComponent } from './after-login/profile-settings/profile-settings.component';
import { SecuritySettingsComponent } from './after-login/security-settings/security-settings.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AfterLoginComponent } from './after-login/after-login.component';
import { PurchaseHistoryComponent } from './after-login/purchase-history/purchase-history.component';
import { OnGoingComponent } from './after-login/exam-module/tab-partials/on-going/on-going.component';
import { CompletedComponent } from './after-login/exam-module/tab-partials/completed/completed.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ExamModuleComponent } from './after-login/exam-module/exam-module.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ExamDetailsComponent } from './after-login/exam-details/exam-details.component';
import { ExamQuestionAnswerComponent } from './after-login/exam-question-answer/exam-question-answer.component';
import { ExamReportComponent } from './after-login/exam-report/exam-report.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PaperPackagesComponent,
    LoginRegistrationComponent,
    PageNotFoundComponent,
    StudentRegistrationActivateAccountComponent,
    StudentRegistrationSuccessComponent,
    StudentResetPasswordComponent,
    ProfileSettingsComponent,
    SecuritySettingsComponent,
    ForgetPasswordComponent,
    AfterLoginComponent,
    PurchaseHistoryComponent,
    OnGoingComponent,
    CompletedComponent,
    NgxMsdImageCropperComponent,
    NgxMsdPaginationComponent,
    ExamModuleComponent,
    ExamDetailsComponent,
    ExamQuestionAnswerComponent,
    ExamReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot()
  ],
  providers: [   
    AppVariables,
    PaperPackagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
