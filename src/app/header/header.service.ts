import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  private subject = new Subject<any>();

  constructor(private _http: HttpClient) { }

  logout(userId, accessToken) {
    let fd = new FormData();
    fd.append("user_email", userId);
    fd.append("user_password", accessToken);

    return this._http.post(environment.apiBaseUrl + 'student-logout', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  setUserData(userData: any) {
    this.subject.next(userData);
  }

  getUserData(): Observable<any> {
    return this.subject.asObservable();
  }

}
