import { Component, OnInit } from '@angular/core';
import { EventEmitter } from 'events';
import { AppVariables } from '../app.variables';
import { HeaderService } from './header.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public userId: string = '';
  public userName: string = '';
  public accessToken: string = '';
  public profilePicUrlJpeg: string = '';

  constructor(public appVariables: AppVariables, private _headerService: HeaderService, private _router: Router, private _toastr: ToastrService) { }

  initJqueryFunctions() {
    // Sticky Navigation
    let lastScrollTop = 0;
    document.addEventListener("scroll", function(e){ 
      let header = document.querySelector('.headerinner')
      let st = window.pageYOffset || document.documentElement.scrollTop; 
      if (st > lastScrollTop) {
        if(window.pageYOffset > 0) {
          header.classList.add('sticky'); 
        }
        if(window.pageYOffset > 500){
          header.classList.add('sticky-hide');
        }
        //console.log('down');
      } else {
        if(window.pageYOffset == 0){
          header.classList.remove('sticky'); 
        }else {
          header.classList.remove('sticky-hide');
        }
          // console.log('up')
      }
      lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
    }, false);

    var height = $('.headerinner').outerHeight();
    $('.headercontainer').css('min-height',height);
    $(window).resize(function(){
      var height = $('.headerinner').outerHeight();
      $('.headercontainer').css('min-height',height);
    });
  }

  ngOnInit(): void {
    let _emitter = new EventEmitter();
    let userData = window.localStorage.getItem('user_data');
    try {
      this.userId = JSON.parse(userData).user_id;
      this.userName = JSON.parse(userData).user_name;
      this.accessToken = JSON.parse(userData).access_token;
      this.profilePicUrlJpeg = this.appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 'c-profile-pic.jpeg?t=' + (new Date().getTime());
    } catch (error) {
    }
    
    //logged in user data
    // _emitter.on('loggedinUserData', (data) => {
    //   console.log('loggedinUserDataEmitter', data);
    //   this.userId = data.user_id;
    //   this.userName = data.user_name;
    //   this.accessToken = data.access_token;
    // });
    this._headerService.getUserData()
    .subscribe((resp) => {
      if(resp == null) {
        this._router.navigateByUrl('/paper-packages');
        window.localStorage.removeItem('user_data');
        this.userId = '';
        this.accessToken = '';
        this._toastr.success(this.appVariables.logoutSuccessMessage, '', {
          timeOut: 5000,
          positionClass:'toast-bottom-right'
        });
      }
      else {
        this.userId = resp.user_id;
        this.userName = resp.user_name;
        this.accessToken = resp.access_token;
        this.profilePicUrlJpeg = this.appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 'c-profile-pic.jpeg?t=' + (new Date().getTime());
      }
    })

    //logged in user data
    _emitter.on('setHeaderProfilePic', () => {
      this.profilePicUrlJpeg = this.appVariables.uploadsPath + 'students/' + this.userId + '/' + this.userId + 'c-profile-pic.jpeg?t=' + (new Date().getTime());
    });

    //set user name
    _emitter.on('setUserName', (data) => {
      this.userName = data;
    });

    //logOut user
    _emitter.on('logOutUser', (data) => {
      // this._headerService.logOutUser(data.userId, data.accessToken)
      // .subscribe((resp) => {        
      //   this.userId = '';
      //   this.userName = '';
      // });
      // window.localStorage.removeItem('user_data');
      // if(window.location.href.indexOf('/customer-dashboard') >= 0) {
      //   this._router.navigateByUrl('/');
      // }
      // // this._router.navigateByUrl('/')
    });
    setTimeout(() => {
      this.initJqueryFunctions();
    }, 500);
  }

  toggleDropdown() {
    $("#UserNavBody").slideToggle("fast");
  }

  logoutClicked() {
    this._headerService.logout(this.userId, this.accessToken)
    .subscribe(resp => {
      this._router.navigateByUrl('/paper-packages');
      this.toggleDropdown();
      window.localStorage.removeItem('user_data');
      this.userId = '';
      this.accessToken = '';
      this._toastr.success(this.appVariables.logoutSuccessMessage, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
    })
  }

}
