import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentRegistrationActivateAccountService } from './student-registration-activate-account.service';
import { AppVariables } from '../app.variables';
// import { EventEmitter } from 'events';


@Component({
  selector: 'app-student-registration-activate-account',
  templateUrl: './student-registration-activate-account.component.html',
  styleUrls: ['./student-registration-activate-account.component.scss']
})
export class StudentRegistrationActivateAccountComponent implements OnInit {

  
  public responseMessageType: number = 0; // 0 => No message to show, 1 => Success, 2 => Error

  constructor(private _activatedRoute: ActivatedRoute, private _studentRegistrationActivateAccountService: StudentRegistrationActivateAccountService, private _router: Router, public appVariables: AppVariables) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((param) => {
      if(typeof param.activationToken === 'undefined' || param.activationToken == '' || param.activationToken == null) {
        // this.responseMessageType = 2;
        // this._router.navigateByUrl('/404', {skipLocationChange: true});
      }
      else {
        // console.log(param)
        this._studentRegistrationActivateAccountService.accountActivate(param.activationToken)
        .subscribe((resp) => {
          if(resp.status == 200) {
            this.responseMessageType = 1;
            setTimeout(() => {
              this._router.navigateByUrl('/');
            }, 6000);
          }
          else {
            // console.log('ok')
            // this._router.navigateByUrl('/404', {skipLocationChange: true});
            this.responseMessageType = 2;
          }
        }, (error) => {
          // this._router.navigateByUrl('/404', {skipLocationChange: true});
          this.responseMessageType = 2;
        });
      }
    });
  }

}
