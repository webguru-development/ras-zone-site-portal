import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class StudentRegistrationActivateAccountService {

  constructor(private _http: HttpClient) { }

  accountActivate(activationToken) {
    let fd = new FormData();
    fd.append("activation_token", activationToken);

    return this._http.post(environment.apiBaseUrl + 'student-account-activation', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

}
