import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class AppVariables {
  constructor() { }
  
  // noImageUrl: string = this._appConfigs.baseUrl + 'images/noimage.jpg';
  // uploadsPath: string = this._appConfigs.baseUrl + 'images/';  

  someErrorOccurred: string = 'Oops! Some error occured.';

  filePathUrl: string = 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/p2/events_partner/public/';
  noImageUrl: string = 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/p2/events_partner/public/images/site-vendors/dummy-user.jpeg';
  filePathUrl_organizer: string = 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/p2/events_partner/public/';
  noImageUrl_organizer: string = 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/p2/events_partner/public/images/site-organizer/dummy-user.jpeg';
  uploadsPath: string = environment.baseUrl + 'images/uploads/';

   // Registration Page Start    
   createTitleValidationError: string = 'Please select your title';
   createFullNameValidationError: string = 'Please enter your full name!';
   createEmailValidationError: string = 'Please enter your valid Email ID!';
   createPhoneNumberValidationError: string = 'Please enter your phone number!';
   createStreetValidationError: string = 'Please enter your street!';
   createCountryValidationError: string = 'Please choose your country!';   
   createStateValidationError: string = 'Please enter your state!';
   createCityValidationError: string = 'Please enter your city!';
   createPinValidationError: string = 'Please enter your valid pin code!';
   createPasswordValidationError: string = 'Please enter your password!';
   createConfirmPasswordValidationError: string = 'Please confirm your password!';
   createPasswordMismatchValidationError: string = 'Password mismatch!';
   createTermsConditionValidationError: string = 'Please accept the terms of use!';
   createDuplicateEmailError: string = 'This Email ID already exists!';
   
   loginEmailValidationError: string = 'Please enter your valid Email ID!';
   loginPasswordValidationError: string = 'Please enter your password!';
   emailNotExistsMessage: string = 'It seems, this email ID either does not registered with us or not activated yet.';
   invalidEmailOrPasswordMessage: string = 'Invalid Email ID or Password';

  // Registration Page Ends

  //Security Settings Page Starts
  securitySettingsInvalidOldPasswordErrorMessage: string = 'Please enter your old password!';
  securitySettingsInvalidNewPasswordErrorMessage: string = 'Please enter your new password!';
  securitySettingsInvalidNewConfirmPasswordErrorMessage: string = 'Please confirm your new password!';
  securitySettingsInvalidMismatchPasswordErrorMessage: string = 'Password mismatch! Please confirm your new password!';
  securitySettingsOldPasswordErrorRespMessage: string = 'Password check your old password!';  
  securitySettingsPasswordChangedSuccessRespMessage: string = 'Password reset successfully!';

  //Security Settings Page Ends
  
 
  // Customer Account Activation Page
  accountActivationSuccess: string = 'Account Activated Successfully!';
  accountActivationError: string = '400 Bad Request!';
  // Customer Account Activation Page

  // Customer Login Page Starts
  loginErrorMessage: string = 'Invalid email or password!';
  loginSuccessMessage: string = 'Logged in successfully!';
  customerLoginInvalidEmailErrorMessage: string = 'Please enter your valid email!';
  customerLoginInvalidPasswordErrorMessage: string = 'Please enter your password!';
  customerLoginForgetPasswordSuccessMessage: string = 'Please check your mail to reset your password!';
  forgotPasswordEmailErrorMessage: string = 'Please enter your valid email!';
  logoutSuccessMessage: string = 'Logged out successfully!';
  //Organizer Login Page Ends
  forgotPasswordEmailErrorMessageorganizer: string = 'Please enter your valid email!';
  // //Vendor Login Page
  vendorLoginInvalidEmailErrorMessage: string = 'Please enter your valid email!';
  vendorLoginInvalidPasswordErrorMessage: string = 'Please enter your password!';
  vendorLoginForgetPasswordSuccessMessage: string = 'Please check your mail to reset your password!';

  // Customer Login Page Starts
  customerResetPasswordInvalidPasswordErrorMessage: string = 'Invalid password!';
  customerResetPasswordInvalidConfirmPasswordErrorMessage: string = 'Password mismatch';
  customerResetPasswordSuccessMessage: string = 'Password reset successfully!';
  //Customer Login Page Ends

  // Profile Settings Page
  profileSettingsSubmittedSuccessfully: string = 'Your profile data has been updated successfully!';
  showErrorEmail: string = 'check your email!';
  // Profile Settings Page

  // Profile Settings Page
  securitySettingsSubmittedSuccessfully: string = 'Your password has been updated successfully!';
  securitySettingsInvalidOldPassword: string = 'Incorrect current password!';
  // Profile Settings Page
  terms_conditionsValidationError='please fillup this field';
  //contact us

  // createFullNameValidationError: string = 'Please enter your full name!';
  // createEmailValidationError: string = 'Please enter your valid email Id!';
  // createPhoneNumberValidationError: string = 'Please enter your phone number!';
  createMessageValidationError:string='please enter your message!';
  showMessage1:string='Your message is saved successfully to our Database..we will connect you shortly';

  showDuplicateEmailError1: string = 'Please enter your Email';
  showPasswordError1: string= 'Please enter your password';

  forgetPassword: string = 'Please enter your registered Email ID.';
  forgetPasswordMailSentSuccess: string = 'A mail with password reset link has been sent to your registered mail ID.';

  resetPasswordPasswordError: string = 'Please enter your new password.';
  resetPasswordPasswordMismatchedError: string = 'Password mismatch.';
  resetPasswordsuccess: string = 'Your password has been changed successfully';

  profileSettingsFullNameError: string = 'Please enter your name.';
  profileSettingsContactNumberError: string = 'Please enter your contact number.';
  profileSettingssuccess: string = 'Changes has been saved successfully';


  questionAnswerPage5MinsRemainingAlertMessage: string = 'You have 5 mins left.';
  questionAnswerPageTimeUpAlertMessage: string = 'Time for this exam is over. Redirecting to report page shortly.';
  
}