import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-registration-success',
  templateUrl: './student-registration-success.component.html',
  styleUrls: ['./student-registration-success.component.scss']
})
export class StudentRegistrationSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
