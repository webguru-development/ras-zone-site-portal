import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ForgetPasswordService {

  constructor(private _http: HttpClient) { }

  sendMailForgetPassword(formData) {
    let fd = new FormData();
    fd.append("user_email", formData.userEmail);

    return this._http.post(environment.apiBaseUrl + 'student-forget-password', fd)
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
