import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ForgetPasswordService } from './forget-password.service';
import { ToastrService } from 'ngx-toastr';
import { AppVariables } from '../app.variables';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  public forgetPasswordForm: FormGroup = this._formBuilder.group({
    'userEmail': ['', [Validators.required]]
  });
  public userEmailError: boolean = false;

  constructor(private _formBuilder: FormBuilder, private _forgetPasswordService: ForgetPasswordService, private _toastr: ToastrService, private _appVariables: AppVariables) { }

  ngOnInit(): void {
  }

  forgetPasswordSubmit() {
    if(this.forgetPasswordForm.value.userEmail == '') {
      this.userEmailError = true;
      this._toastr.error(this._appVariables.forgetPassword, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
      return false;
    }
    
    this._forgetPasswordService.sendMailForgetPassword(this.forgetPasswordForm.value)
    .subscribe((resp) => {
      console.log(resp);
      this._toastr.success(this._appVariables.forgetPasswordMailSentSuccess, '', {
        timeOut: 5000,
        positionClass:'toast-bottom-right'
      });
    });
  }

}
